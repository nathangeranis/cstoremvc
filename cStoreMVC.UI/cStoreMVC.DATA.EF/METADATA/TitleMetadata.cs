﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//This namespace and the Title.cs (EDMX) namespace MUST be the same.

namespace cStoreMVC.DATA.EF//.Metadata
{
    //Make it public
    public class TitleMetadata
    {
        //We don't need to apply metadata to the Primary Key,
        //so comment it out
        //public int TitleID { get; set; }
        [Display(Name = "Title")]
        [Required(ErrorMessage = "*Required")]
        [StringLength(100, ErrorMessage = "Title should not exceed 50 characters")]
        public string BookTitle { get; set; }
        [StringLength(14, ErrorMessage = "ISBN should not exceed 14 characters")]
        [DisplayFormat(NullDisplayText = "N/A")]
        public string ISBN { get; set; }
        [DisplayFormat(NullDisplayText = "N/A")]
        [UIHint("MultilineText")] //THIS SHOULD BE DONE BEFORE SCAFFOLDING
        public string Description { get; set; }
        //public Nullable<int> GenreID { get; set; }
        [DisplayFormat(NullDisplayText = "Free", DataFormatString = "{0:c}")]
        public Nullable<decimal> Price { get; set; }
        [DisplayFormat(NullDisplayText = "N/A", DataFormatString = "{0:n0}")]
        [Display(Name = "Units Sold")]
        [Range(0, int.MaxValue, ErrorMessage = "*")]
        public Nullable<int> UnitsSold { get; set; }
        [DisplayFormat(NullDisplayText = "N/A", DataFormatString = "{0:d}")]
        [Display(Name = "Publish Date")]
        public Nullable<System.DateTime> PublishDate { get; set; }
        //public int PublisherID { get; set; }
        //Boolean values do NOT get required validation - they are
        //represented in the UI as a checkbox - unchecked
        //check translates to false, checked translates to true
        //if you have nullable<bool> the UI represents with a Dropdown
        //list for all 3 possibilities(null, true, false)
        [Display(Name = "Cover Image")]
        public string BookImage { get; set; }
        [Display(Name = "Site Feature")]
        public bool IsSiteFeature { get; set; }
        [Display(Name = "Genre Feature")]
        public bool IsGenreFeature { get; set; }
        [Display(Name = "EBook")]
        public bool IsEBook { get; set; }

        //These are the properities of the Title Class that represent
        //the Navigational properties in the EDMX designer
        //(at the bottom of each table, they represent PK-> FK relationships)
        //public virtual Genre Genre { get; set; }
        //public virtual Publisher Publisher { get; set; }
        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        //public virtual ICollection<TitleAuthor> TitleAuthors { get; set; }
    }//end class

    //This step is what applies our metadata (above) to the class.
    [MetadataType(typeof(TitleMetadata))]
    public partial class Title { }
}//end namespace
