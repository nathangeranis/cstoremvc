﻿using cStoreMVC.DATA.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cStoreMVC.DOMAIN.Repositories
{
    #region Entity Specific Repository - AuthorRepository

    //public class AuthorRepository
    //{
    //    //LAB!
    //    //Add the 5 methods for Authors in the repository
    //    //and implement the repository in the Controller.

    //    private cStoreEntities db = new cStoreEntities();

    //    public List<Author> Get()
    //    {
    //        return db.Authors.ToList();
    //    }//end Get()

    //    public Author Find(int? id)
    //    {
    //        return db.Authors.Find(id);
    //    }//Find

    //    public void Add(Author author)
    //    {
    //        db.Authors.Add(author);
    //        db.SaveChanges();
    //    }//Add

    //    public void Update(Author author)
    //    {
    //        db.Entry(author).State = System.Data.Entity.EntityState.Modified;
    //        db.SaveChanges();

    //    }
    //    public void Remove(Author author)
    //    {
    //        db.Authors.Remove(author);
    //        db.SaveChanges();
    //    }

    //    private bool disposed = false;

    //    protected virtual void Dispose(bool disposing)
    //    {
    //        if (!this.disposed)
    //        {
    //            if (disposing)
    //            {
    //                db.Dispose();
    //            }//end disposing
    //        }//end !disposed
    //    }//end Dispose()
    //    public void Dispose()
    //    {
    //        Dispose(true);
    //        GC.SuppressFinalize(this);
    //    }

    //}//end class
    #endregion
    //an Interface could go here.

    //Lab!
    //1) Create an IAuthorRepository interface that requires a method called ForeighAuthors() - 
    //The method should return a collection of authors that do NOT live in the United States
    //2) Implement the interface on the AuthorsRepository and then write the logic for ForeignAuthors()
    //3) Create an action that returns a View of all foreign authors to display the results

    public interface IAuthorRepository<TEntity>
    {
        List<Author> ForeignAuthors();
    }//end class

    public class AuthorRepository : GenericRepository<Author>
    {
        private cStoreEntities db = new cStoreEntities();
        public virtual List<Author> ForeignAuthors()
        {

        }

    }
}//end namespace
