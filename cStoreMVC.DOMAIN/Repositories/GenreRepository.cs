﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using cStoreMVC.DATA.EF;


namespace cStoreMVC.DOMAIN.Repositories
{
    #region Entity Specific Repository - GenreRepository

    //public class GenreRepository
    //{
    //    //This is an entity specific repository, it only pertains to the Genres table

    //    //CRUD functions that are built when we scaffold a Controller
    //    //1. Lost a records (table)
    //    //2. Find (individual record by ID)
    //    //3. add (add a record)
    //    //4. Edit (edits a record/updates a record, uses the Find())
    //    //5. Remove (removes a record, uses the Find())

    //    /*Step 1. Connection to the db via EDMX*/
    //    private cStoreEntities db = new cStoreEntities();

    //    /*Step 2. Create Methods for CRUD functions (5 total)*/
    //    public List<Genre> Get()
    //    {
    //        //Get all Genres in dB and send them to a list
    //        return db.Genres.ToList();
    //    }//Get

    //    public Genre Find(int? id)
    //    {
    //        return db.Genres.Find(id);
    //    }//Find

    //    public void Add(Genre genre)
    //    {
    //        db.Genres.Add(genre);
    //        db.SaveChanges();
    //    }//Add

    //    public void Update (Genre genre)
    //    {
    //        db.Entry(genre).State = System.Data.Entity.EntityState.Modified;
    //        //EntityState.Modified is a flag to send all fields/values for the record
    //        //to the datastructure
    //        db.SaveChanges();

    //    }
    //    public void Remove(Genre genre)
    //    {
    //        db.Genres.Remove(genre);
    //        db.SaveChanges();
    //    }

    //    private bool disposed = false;
    //    //"free-up" resources pertaining to an instance of GenreRepository

    //    protected virtual void Dispose(bool disposing)
    //    {
    //        if (!this.disposed)
    //        {
    //            if (disposing)
    //            {
    //                db.Dispose();
    //            }//end disposing
    //        }//end !disposed
    //    }//end Dispose()
    //    public void Dispose()
    //    {
    //        Dispose(true);
    //        GC.SuppressFinalize(this);
    //    }
    //}//end class
    #endregion
    public class GenreRepository : GenericRepository<Genre>
    {

    }
}//end namespace
