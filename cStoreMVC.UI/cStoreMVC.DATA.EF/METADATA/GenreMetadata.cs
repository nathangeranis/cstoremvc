﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace cStoreMVC.DATA.EF.METADATA
{
    public class GenreMetadata
    {
        [Required(ErrorMessage = "* Required")]
        [StringLength(15, ErrorMessage = "* Cannot exceed 15 characters")]
        [Display(Name = "Genre Name")]
        public string GenreName { get; set; }

    }//end class
    [MetadataType(typeof(GenreMetadata))]
    public partial class Genre { }
}//end namespace
