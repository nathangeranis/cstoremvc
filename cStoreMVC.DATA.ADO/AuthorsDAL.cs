﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient; //Added for SqlClient classes
// Provides access to all SqlClient classes needed for data retrieval/manupulation in a MSSQL dB
//(SqlConnection, SqlCommand, SqlDataReader, SqlParameter)
using cStoreMVC.DOMAIN.Models;
using System.Web.Configuration; //Added Physical reference to System.Web AND System.Configuration
//allowing us access to .Configuration for WebConfigurationManager
namespace cStoreMVC.DATA.ADO
{
    //DAL is Data Access Layer
    //Any items, structures, commands to access data should live here.
    public class AuthorsDAL
    {
        //We want a field that represents the connection string
        //.WebConfigElement["Name of the Connection"].Property;
        string connString = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        #region Adding Data to our Website
        /*
         * ADO.NET - ActiveX Data Objects
         * -integration of data into ASP.NET websites (Active Server Pages)
         * -uses SQLClient classes (.NET Framework, C#)
         * 
         * 1. Where is the Data?
         * 2. Send a request for the data/What do we want? (SELECT)
         * 
         */
        #endregion
        public string GetFirstNames()
        {
            string firstNames = "";
            //1. Create a connection to the dB (SqlConnection)
            //DataSource = dB Server (locally .\SQLexpress)
            //Initial Catalog = dBName
            //Integrated Security = true|false|sspi

            //Create an empty instance of the SqlConnection object

            SqlConnection conn = new SqlConnection();
            //Define the ConnectionString properties
            conn.ConnectionString = @"Data Source=.\sqlexpress;Initial Catalog=cStore;Integrated Security = true;";
            //In C#, @ is used for the string literal (disregards all escapes)

            //2. Open the connection - once opened, it is "in use" and not available to a user until
            //it gets closed.
            conn.Open();

            //3.Create the SqlCommand statement to get First Names
            //(Select, Insert, Update, Delete)
            SqlCommand cmdGetAuthors = new SqlCommand("SELECT * FROM Authors",conn);
            //T-SQL insIDe of a C# string, SQL syntax applies

            //4. Execute the command
            //Use SqlCommand object to call methods for execution
            // .ExecuteReader() - for SELECTing Data from the dB
            //(read-only, cannot manipulate data)
            //.ExecuteNonQuery() - for everything else
            //(INSERT, UPDATE, DELETE)
            //.ExecuteScalar() - for aggregates (COUNT, SUM, AVG)

            SqlDataReader rdrAuthors = cmdGetAuthors.ExecuteReader();
            //We hold the results in a SqlDataReader object since it is the return type of the .ExecuteReader()

            //5. Process the DataReader
            //.Read() will check to see if there are records in the DataReader and will advance the cursor to that record.
            while (rdrAuthors.Read())
            {
                //while there is Data to read
                //Add current value to firstNames variable
                firstNames += (string)rdrAuthors["FirstName"] + ", ";
            }

            //6. Close the data Reader and db connection
            rdrAuthors.Close();//Close DRO
            conn.Close();//Close Connection

            //7. Return the results


            return firstNames;
        }//GetFirstNames()

        public List<AuthorModel> GetAuthors()
        {
            List<AuthorModel> authors = new List<AuthorModel>();

            using (SqlConnection conn = new SqlConnection())
            {
                //Do NOT want open dB connections "hanging around".....
                //Wrapping the dB connection in a using block allows for automatically closing the 
                //connection once it's no longer being used

                //Using () will automatically close the connection object, however we still 
                //need to manually open the connection
                //conn.ConnectionString = @"Data Source=.\sqlexpress;Initial Catalog=cStore;Integrated Security=true;";
                //Now using updated ConnectionString object.
                conn.ConnectionString = connString;
                conn.Open();
                SqlCommand cmdGetAllAuthors = new SqlCommand("SELECT * FROM Authors", conn);

                //Execute the command and store the results

                SqlDataReader rdrAuthors = cmdGetAllAuthors.ExecuteReader();
                while (rdrAuthors.Read())
                {
                    //create an AuthorModel object that can hold all of the info for an author
                    AuthorModel author = new AuthorModel()
                    {
                        //Assign values to AuthorModel properties
                        AuthorID = (int)rdrAuthors["AuthorID"],
                        FirstName = (string)rdrAuthors["FirstName"],
                        LastName = (string)rdrAuthors["LastName"],
                        City = (rdrAuthors["City"] is DBNull) ? "N/A" : (string)rdrAuthors["City"],
                        State = (rdrAuthors["State"] is DBNull) ? "N/A" : (string)rdrAuthors["State"],
                        ZipCode = (rdrAuthors["ZipCode"] is DBNull) ? "N/A" : (string)rdrAuthors["ZipCode"],
                        Country = (rdrAuthors["Country"] is DBNull) ? "N/A" : (string)rdrAuthors["Country"]

                    };//end Author
                    authors.Add(author);
                }//end while
                rdrAuthors.Close();
            }//using





            return authors;
        }//GetAuthors()
        public void CreateAuthor(AuthorModel author)
        {
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                SqlCommand cmdInsertAuthor = new SqlCommand(
                    "INSERT INTO Authors(FirstName, LastName, City, State, ZipCode, Country)" +
                    "VALUES(@FirstName, @LastName, @City, @State, @ZipCode, @Country)", conn);
                //Creating a SqlParameter object to pass into ADD()
                //SqlParameter FirstName = new SqlParameter("@FirstName", author.FirstName);
                //cmdInsertAuthor.Parameters.Add(FirstName);

                //SqlParameter LastName = new SqlParameter("@LastName", author.LastName);
                //cmdInsertAuthor.Parameters.Add(LastName);

                //SqlParameter City = new SqlParameter("@City", author.City);
                //cmdInsertAuthor.Parameters.Add(City);

                //SqlParameter State = new SqlParameter("@State", author.State);
                //cmdInsertAuthor.Parameters.Add(State);

                //SqlParameter ZipCode = new SqlParameter("@ZipCode", author.ZipCode);
                //cmdInsertAuthor.Parameters.Add(ZipCode);

                //Add variables + values into the SqlCommand object
                //Seen frequently in production level code

                cmdInsertAuthor.Parameters.AddWithValue("@AuthorID", author.AuthorID);
                cmdInsertAuthor.Parameters.AddWithValue("@FirstName", author.FirstName);
                cmdInsertAuthor.Parameters.AddWithValue("@LastName", author.LastName);

                //Nullable Fields
                if (author.City != null)
                {
                    cmdInsertAuthor.Parameters.AddWithValue("@City", author.City);
                }           
                else        
                {           
                    cmdInsertAuthor.Parameters.AddWithValue("@City", DBNull.Value);
                }      
                if (author.State != null)
                {
                    cmdInsertAuthor.Parameters.AddWithValue("@State", author.State);
                }
                else
                {
                    cmdInsertAuthor.Parameters.AddWithValue("@State", DBNull.Value);
                }
                if (author.ZipCode != null)
                {
                    cmdInsertAuthor.Parameters.AddWithValue("@ZipCode", author.ZipCode);

                }
                else
                {
                    cmdInsertAuthor.Parameters.AddWithValue("@ZipCode", DBNull.Value);
                }
                if (author.Country != null)
                {
                    cmdInsertAuthor.Parameters.AddWithValue("@Country", author.Country);

                }
                else
                {
                    cmdInsertAuthor.Parameters.AddWithValue("@Country", DBNull.Value);
                }

                cmdInsertAuthor.ExecuteNonQuery();
            }//using
        }//CreateAuthor()

        public AuthorModel FindAuthor(int ID)
        {
            //Accept an ID as a parameter to know which author to find (or to update)
            AuthorModel author = null;
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                SqlCommand cmdFindAuthor = new SqlCommand(
                    "SELECT * FROM Authors WHERE AuthorID = @AuthorID", conn);
                //Add SqlParameter for @AuthorID
                cmdFindAuthor.Parameters.AddWithValue("@AuthorID", ID);
                //Execute the command
                SqlDataReader rdr = cmdFindAuthor.ExecuteReader();
                if (rdr.Read())
                {
                    //An author was returned and we should build our AuthorModel object
                    author = new AuthorModel()
                    {
                        AuthorID = (int)rdr["AuthorID"],
                        FirstName = (string)rdr["FirstName"],
                        LastName = (string)rdr["LastName"],


                        City = (rdr["City"] is DBNull) ? "N/A" : (string)rdr["City"],
                        State = (rdr["State"] is DBNull) ? "N/A" : (string)rdr["State"],
                        ZipCode = (rdr["ZipCode"] is DBNull) ? "N/A" : (string)rdr["ZipCode"],
                        Country = (rdr["Country"] is DBNull) ? "N/A" : (string)rdr["Country"]

                    };
                }
                rdr.Close();
            }
            return author;
        }//end FindAuthor()

        public void UpdateAuthor(AuthorModel author)
        {
            using (SqlConnection conn = new SqlConnection(connString))
            {
                SqlCommand cmdUpdateAuthor = new SqlCommand(
                    @"UPDATE Authors SET
                        FirstName = @FirstName, 
                        LastName = @LastName,
                        City = @City,
                        State = @State,
                        ZipCode = @ZipCode,
                        Country = @Country
                        WHERE AuthorID = @AuthorID", conn);
                cmdUpdateAuthor.Parameters.AddWithValue("@AuthorID", author.AuthorID);
                cmdUpdateAuthor.Parameters.AddWithValue("@FirstName", author.FirstName);
                cmdUpdateAuthor.Parameters.AddWithValue("@LastName", author.LastName);

                //Nullable Fields
                if (author.City != null)
                {
                    cmdUpdateAuthor.Parameters.AddWithValue("@City", author.City);
                }
                else
                {
                    cmdUpdateAuthor.Parameters.AddWithValue("@City", DBNull.Value);
                }
                if (author.State != null)
                {
                    cmdUpdateAuthor.Parameters.AddWithValue("@State", author.State);
                }
                else
                {
                    cmdUpdateAuthor.Parameters.AddWithValue("@State", DBNull.Value);
                }
                if (author.ZipCode != null)
                {
                    cmdUpdateAuthor.Parameters.AddWithValue("@ZipCode", author.ZipCode);

                }
                else
                {
                    cmdUpdateAuthor.Parameters.AddWithValue("@ZipCode", DBNull.Value);
                }
                if (author.Country != null)
                {
                    cmdUpdateAuthor.Parameters.AddWithValue("@Country", author.Country);

                }
                else
                {
                    cmdUpdateAuthor.Parameters.AddWithValue("@Country", author.Country);
                }
                conn.Open();
                cmdUpdateAuthor.ExecuteNonQuery();
            }//using
        }//UpdateAuthor
        public void DeleteAuthor(int id)
        {
            /*
             * HARD DELETE: 
             * DeleteAuthor() is a HARD Delete - Meaning, this will remove the item from the dB as long as
             * it is NOT bound by a primary/foreign key relationship.
             * 
             *SOFT DELETE:
             *REALLY just an update statement that changes the status field in a table. The field could be a bit value 
             * in the table (ex. discontinued-Northwind) or it could be a foreign key to a lookup table ex. AcctStatusCode
             * -F2071 Bank) *****In the case of foreign key, it takes a little more work in the update (more logic)
             * 
             */

            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();

                SqlCommand cmdDeleteAuthor = new SqlCommand("DELETE FROM Authors WHERE AuthorID = @AuthorID", conn);
                /*******************DO NOT FORGET YOUR WHERE CLAUSE***********************/
                cmdDeleteAuthor.Parameters.AddWithValue("@AuthorID", id);
                cmdDeleteAuthor.ExecuteNonQuery();
            }//using
        }
    }//end class
}//end namespace
