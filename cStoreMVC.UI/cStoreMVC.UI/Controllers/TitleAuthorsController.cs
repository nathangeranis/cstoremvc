﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using cStoreMVC.DATA.EF;

namespace cStoreMVC.UI.Controllers
{
    public class TitleAuthorsController : Controller
    {
        private cStoreEntities db = new cStoreEntities();

        // GET: TitleAuthors
        public ActionResult Index()
        {
            var titleAuthors = db.TitleAuthors.Include(t => t.Author).Include(t => t.Title);
            return View(titleAuthors.ToList());
        }

        // GET: TitleAuthors/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TitleAuthor titleAuthor = db.TitleAuthors.Find(id);
            if (titleAuthor == null)
            {
                return HttpNotFound();
            }
            return View(titleAuthor);
        }

        // GET: TitleAuthors/Create
        public ActionResult Create()
        {
            ViewBag.AuthorID = new SelectList(db.Authors, "AuthorID", "FirstName");
            ViewBag.TitleID = new SelectList(db.Titles, "TitleID", "BookTitle");
            return View();
        }

        // POST: TitleAuthors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TitleAuthorID,TitleID,AuthorID,AuthorOrder")] TitleAuthor titleAuthor)
        {
            if (ModelState.IsValid)
            {
                db.TitleAuthors.Add(titleAuthor);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AuthorID = new SelectList(db.Authors, "AuthorID", "FirstName", titleAuthor.AuthorID);
            ViewBag.TitleID = new SelectList(db.Titles, "TitleID", "BookTitle", titleAuthor.TitleID);
            return View(titleAuthor);
        }

        // GET: TitleAuthors/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TitleAuthor titleAuthor = db.TitleAuthors.Find(id);
            if (titleAuthor == null)
            {
                return HttpNotFound();
            }
            ViewBag.AuthorID = new SelectList(db.Authors, "AuthorID", "FirstName", titleAuthor.AuthorID);
            ViewBag.TitleID = new SelectList(db.Titles, "TitleID", "BookTitle", titleAuthor.TitleID);
            return View(titleAuthor);
        }

        // POST: TitleAuthors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TitleAuthorID,TitleID,AuthorID,AuthorOrder")] TitleAuthor titleAuthor)
        {
            if (ModelState.IsValid)
            {
                db.Entry(titleAuthor).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AuthorID = new SelectList(db.Authors, "AuthorID", "FirstName", titleAuthor.AuthorID);
            ViewBag.TitleID = new SelectList(db.Titles, "TitleID", "BookTitle", titleAuthor.TitleID);
            return View(titleAuthor);
        }

        // GET: TitleAuthors/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TitleAuthor titleAuthor = db.TitleAuthors.Find(id);
            if (titleAuthor == null)
            {
                return HttpNotFound();
            }
            return View(titleAuthor);
        }

        // POST: TitleAuthors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TitleAuthor titleAuthor = db.TitleAuthors.Find(id);
            db.TitleAuthors.Remove(titleAuthor);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
