﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace cStoreMVC.DATA.EF.METADATA
{
    public class PublisherMetadata
    {
        public string PublisherName { get; set; }
        public string City { get; set; }
        public string State { get; set; }
    }//end class
    [MetadataType(typeof(PublisherMetadata))]
    public partial class Publisher { }
}//end namespace
