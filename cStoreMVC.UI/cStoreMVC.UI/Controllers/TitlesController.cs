﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using cStoreMVC.DATA.EF;
using cStoreMVC.DOMAIN.Repositories;

namespace cStoreMVC.UI.Controllers
{
    public class TitlesController : Controller
    {
        //private cStoreEntities db = new cStoreEntities();
        TitleRepository repoT = new TitleRepository();
        GenreRepository repoG = new GenreRepository();
        PublisherRepository repoP = new PublisherRepository();
        // GET: Titles
        public ActionResult Index()
        {
            var titles = repoT.Get("Genre,Publisher");
            return View(titles.ToList());
        }

        // GET: Titles/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Title title = repoT.Find(id);
            if (title == null)
            {
                return HttpNotFound();
            }
            return View(title);
        }

        // GET: Titles/Create
        public ActionResult Create()
        {
            ViewBag.GenreID = new SelectList(repoG.Get(), "GenreID", "GenreName");
            ViewBag.PublisherID = new SelectList(repoP.Get(), "PublisherID", "PublisherName");
            return View();
        }

        // POST: Titles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TitleID,BookTitle,ISBN,Description,GenreID,Price,UnitsSold,PublishDate,PublisherID,BookImage,IsSiteFeature,IsGenreFeature,IsEBook")] Title title)
        {
            if (ModelState.IsValid)
            {
                repoT.Add(title);
                //db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.GenreID = new SelectList(repoG.Get(), "GenreID", "GenreName");
            ViewBag.PublisherID = new SelectList(repoP.Get(), "PublisherID", "PublisherName");
            return View(title);
        }

        // GET: Titles/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Title title = repoT.Find(id);
            if (title == null)
            {
                return HttpNotFound();
            }
            ViewBag.GenreID = new SelectList(repoG.Get(), "GenreID", "GenreName");
            ViewBag.PublisherID = new SelectList(repoP.Get(), "PublisherID", "PublisherName");
            return View(title);
        }

        // POST: Titles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TitleID,BookTitle,ISBN,Description,GenreID,Price,UnitsSold,PublishDate,PublisherID,BookImage,IsSiteFeature,IsGenreFeature,IsEBook")] Title title)
        {
            if (ModelState.IsValid)
            {
                repoT.Update(title);
                //db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.GenreID = new SelectList(repoG.Get(), "GenreID", "GenreName");
            ViewBag.PublisherID = new SelectList(repoP.Get(), "PublisherID", "PublisherName");
            return View(title);
        }

        // GET: Titles/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Title title = repoT.Find(id);
            if (title == null)
            {
                return HttpNotFound();
            }
            return View(title);
        }

        // POST: Titles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Title title = repoT.Find(id);
            repoT.Remove(title);
            //db.SaveChanges();
            return RedirectToAction("Index");
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
