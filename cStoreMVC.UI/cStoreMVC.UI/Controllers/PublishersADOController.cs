﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using cStoreMVC.DATA.ADO;
using cStoreMVC.DOMAIN.Models;

namespace cStoreMVC.UI.Controllers
{
    public class PublishersADOController : Controller
    {
        //Createa an instance of the Data Access Layer (class)
        PublishersDAL publishers = new PublishersDAL();

        // GET: PublishersADO
        public ActionResult Index()
        {
            //Return all publisher first names using DAL method
            ViewBag.PublisherNames = publishers.GetPublisherNames();
            return View();
        }

        public ActionResult GetPublishers()
        {
            return View(publishers.GetPublishers());
        }//GetPublishers()

        //NEEDS GET AND POST, Creates an empty form for the end user to add an Publisher
        public ActionResult CreatePublisher()
        {

            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]

        public ActionResult CreatePublisher(PublisherModel Publisher)
        {
            if (ModelState.IsValid)//ServerSideValidation
            {
                //Call to our PublishersDAL object to use the 
                //CreatePublisher() which will create an Publisher
                //with the values passed from the view
                publishers.CreatePublisher(Publisher);
                return RedirectToAction("GetPublishers");
            }//if ModelState.IsValid

            //If the end user entered BAD Publisher info, send them back to the 
            //CreatePublisher view with the info that was entered and show validation errors on the client

            return View(Publisher);
        }
        public ActionResult PublisherDetails(int id)
        {
            //Create an publisher object and populate it's values using the FindPublisher() to display on the view
            PublisherModel publisher = publishers.FindPublisher(id);
            return View(publisher);
        }

        public ActionResult UpdatePublisher(int id)
        {
            //Return the publisher object AND populate the form
            return View(publishers.FindPublisher(id));
        }//updatePublisher()
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdatePublisher(PublisherModel publisher)
        {
            if (ModelState.IsValid)
            {
                publishers.UpdatePublisher(publisher);
                //Send the end user back to the list of publishers
                return RedirectToAction("GetPublishers");
            }
            return View(publisher);
        }

        public ActionResult DeletePublisher(int id)
        {
            publishers.DeletePublisher(id);
            return RedirectToAction("GetPublishers");
        }//DeletePublisher


    }//end class
}//end namespace