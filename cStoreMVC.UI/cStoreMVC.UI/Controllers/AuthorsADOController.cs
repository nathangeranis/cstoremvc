﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using cStoreMVC.DATA.ADO;
using cStoreMVC.DOMAIN.Models;

namespace cStoreMVC.UI.Controllers
{
    public class AuthorsADOController : Controller
    {
        //Createa an instance of the Data Access Layer (class)
        AuthorsDAL authors = new AuthorsDAL();

        // GET: AuthorsADO
        public ActionResult Index()
        {
            //Return all author first names using DAL method
            ViewBag.FirstNames = authors.GetFirstNames();
            return View();
        }
        public ActionResult GetAuthors()
        {
            //
            return View(authors.GetAuthors());
        }//GetAuthors()

        //NEEDS GET AND POST, Creates an empty form for the end user to add an Author
        public ActionResult CreateAuthor()
        {

            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]

        public ActionResult CreateAuthor(AuthorModel author)
        {
            if (ModelState.IsValid)//ServerSideValidation
            {
                //Call to our AuthorsDAL object to use the 
                //CreateAuthor() which will create an author
                //with the values passed from the view
                authors.CreateAuthor(author);
                return RedirectToAction("GetAuthors");
            }//if ModelState.IsValid

            //If the end user entered BAD author info, send them back to the 
            //CreateAuthor view with the info that was entered and show validation errors on the client

            return View(author);
        }

        public ActionResult AuthorDetails(int id)
        {
            //Create an author object and populate it's values using the FindAuthor() to display on the view
            AuthorModel author = authors.FindAuthor(id);
            return View(author);
        }

        public ActionResult UpdateAuthor(int id)
        {
            //Return the author object AND populate the form
            return View(authors.FindAuthor(id));
        }//updateAuthor()
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateAuthor(AuthorModel author)
        {
            if (ModelState.IsValid)
            {
                authors.UpdateAuthor(author);
                //Send the end user back to the list of authors
                return RedirectToAction("GetAuthors");
            }
            return View(author);
        }
        public ActionResult DeleteAuthor (int id)
        {
            authors.DeleteAuthor(id);
            return RedirectToAction("GetAuthors");
        }//DeleteAuthor

        public ActionResult LinqExamples()
        {
            /*
             * LINQ (Language INtegrated Query) is a technology used to interact with data objects.
             * There are 2 different syntax that we use with LINQ
             * 
             * Method Syntax - uses objects and C# methods to interact with data
             * 
             * Query Syntax - C# Keywords written in SQL execution order
             */
            #region NY State Authors
            //Get all authors from NY state

            //Method Syntax
            //var nyAuthors = authors.GetAuthors().Where(a => a.State == "NY");

            //Query Syntax 
            var nyAuthors = (from a in authors.GetAuthors() where a.State == "NY" select a).OrderBy(a => a.LastName);
            //return View(nyAuthors);
            #endregion
            #region ViewBag Property Example
            var firstNames = (from a in authors.GetAuthors() select a.FirstName).ToList();

            ViewBag.LinqFirstNames = firstNames;
            return View();
            #endregion
        }//LinqExamples()
    }//end class
}//end namespace