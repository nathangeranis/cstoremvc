﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace cStoreMVC.DATA.EF
{
    public class AuthorMetadata
    {
        [Required(ErrorMessage = "* Required")]
        [StringLength(15, ErrorMessage = "* Cannot exceed 15 characters")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "* Required")]
        [StringLength(15, ErrorMessage = "* Cannot exceed 15 characters")]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [StringLength(50, ErrorMessage ="* Cannot exceed 50 characters")]
        public string City { get; set; }

        [StringLength(2, ErrorMessage ="* Enter 2 digit state abbreviation")]
        public string State { get; set; }

        [StringLength(6, ErrorMessage ="* Cannot exceed 6 characters")]
        [Display(Name ="Zip Code")]
        public string ZipCode { get; set; }

        [StringLength(30, ErrorMessage ="* Cannot exceed 30 characters")]
        public string Country { get; set; }

    }//end class
    [MetadataType(typeof(AuthorMetadata))]
    public partial class Author { }

}//end namespace
