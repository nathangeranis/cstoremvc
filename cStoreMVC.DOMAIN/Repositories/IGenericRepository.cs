﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cStoreMVC.DOMAIN.Repositories
{
    //Interfaces allow devs to define/specify the methods that a class must have in order to be used.
    //Interfaces DO NOT define functionality, only ensure a method signature - (method name, params, return type)
    //exists in a class.

    public interface IGenericRepository<TEntity> : IDisposable where TEntity : class
    {
        List<TEntity> Get(string includeProperties = "");
        TEntity Find(object id);
        void Add(TEntity entity);
        void Update(TEntity entity);
        void Remove(object id);
        void Remove(TEntity entity);

        int countRecords();
    }//end class
}//end namespace
