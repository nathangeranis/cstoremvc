﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cStoreMVC.DOMAIN.Models
{
    #region What is the Domain Layer?
    /*
     * Classes in the Domain Layer represent tables (and their fields), referred to as "domains" in the datastructure.
     * When we define models, we should name them as [TableName]Model. When we declare properties of this class
     * they will map to the FIELDS in the table, and the data type for each class property should match (C# equivalent)
     * 
     * NOTE: You will find it useful to have the dB table definitions available to check SQL datatypes. You can use your SQL & C#
     * DataTypes chart from DATA to find appropriate datatypes to use in either scenario (C# -> SQL and SQL-> C#)
     * 
     */
    #endregion
    public class AuthorModel //this is a domain model
    {
        //fields
        //properties
        [Key] //Primary Key- only happens with ConnectedSql, Not NEEDED for LinqToSql or LinqToEntity (EF)
        public int AuthorID { get; set; }

        [Required(ErrorMessage ="* Required")]
        [StringLength(15, ErrorMessage = "FirstName cannot exceed 15 Characters")]
        [Display(Name ="First Name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "* Required")]
        [StringLength(15, ErrorMessage = "LastName cannot exceed 15 Characters")]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [StringLength(50, ErrorMessage = "City cannot exceed 50 Characters")]
        public string City { get; set; }

        [StringLength(2, ErrorMessage = "State cannot exceed 2 characters")]
        public string State { get; set; }

        [StringLength(6, ErrorMessage = "Zip Code cannot exceed 6 characters")]
        [Display(Name ="Zip Code")]
        public string ZipCode { get; set; }

        [StringLength(30, ErrorMessage = "Country cannot exceed 30 characters")]
        public string Country { get; set; }

        /*
         * Other Useful Data Attributes
         * [UIHint("MultilineText")] - used to display textarea vs textbox
         * [Range(begVal, endVal, ErrorMessage)] - 
         */

        //ctors
        //methods

    }//end class
}//end namespace
