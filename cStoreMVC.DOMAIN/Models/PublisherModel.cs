﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cStoreMVC.DOMAIN.Models
{
    public class PublisherModel //this is a domain model
    {
        //fields
        //properties
        [Key] //Primary Key- only happens with ConnectedSql, Not NEEDED for LinqToSql or LinqToEntity (EF)
        public int PublisherID { get; set; }

        [Required(ErrorMessage = "* Required")]
        [StringLength(50, ErrorMessage = "Publisher Name cannot exceed 50 Characters")]
        [Display(Name = "Publisher Name")]
        public string PublisherName { get; set; }

        [StringLength(20, ErrorMessage = "City cannot exceed 20 Characters")]
        public string City { get; set; }

        [StringLength(2, ErrorMessage = "State cannot exceed 2 characters")]
        public string State { get; set; }


        /*
         * Other Useful Data Attributes
         * [UIHint("MultilineText")] - used to display textarea vs textbox
         * [Range(begVal, endVal, ErrorMessage)] - 
         */

        //ctors
        //methods

    }//end class
}//end namespace
