﻿function validateForm(event) {
    //With custom JS validation, we will require each field
    var name = document.forms['main-contact-form']['name'].value;
    var email = document.forms['main-contact-form']['email'].value;
    var subject = document.forms['main-contact-form']['subject'].value;
    var message = document.forms['main-contact-form']['message'].value;
    //Get error message <span/>
    var nameVal = document.getElementById('nameVal');
    var emailVal = document.getElementById('emailVal');
    var subjectVal = document.getElementById('subjectVal');
    var messageVal = document.getElementById('messageVal');
    //Test all of our conditions including checking for a valid email
    var emailRegEx = new RegExp(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/);
    var nameRegEx = new RegExp(/^[A-z]+$/);
    if (name.length == 0 || email.length == 0 || subject.length == 0 ||
        message.length == 0 || !emailRegEx.test(email) || !nameRegEx.test(name))
    {
        //Error messages for required fields
        if (name.length == 0)
        {
            nameVal.textContent = "*Name is required";
        } else {
            nameVal.textContent = "";
        }
        if (email.length == 0) {
            emailVal.textContent = "*Email is required";
        } else {
            emailVal.textContent = "";
        }
        if (subject.length == 0) {
            subjectVal.textContent = "*Subject is required";
        } else {
            subjectVal.textContent = "";
        }
        if (message.length == 0) {
            messageVal.textContent = "*Message is required";
        }else {
            messageVal.textContent = "";
        }
        if (!emailRegEx.test(email) && email.length > 0)
        {
            emailVal.textContent = "*Must be a valid email address";
        }
        if (emailRegEx.test(email) && email.length > 0)
        {
            emailVal.textContent = "";
        }
        if (!nameRegEx.test(name) && name.length > 0) {
            nameVal.textContent = "*Name must not include numbers";
        }
        if (nameRegEx.test(name) && name.length > 0) {
            nameVal.textContent = "";
        }

        event.preventDefault();
    }
}