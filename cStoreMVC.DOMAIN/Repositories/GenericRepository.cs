﻿using cStoreMVC.DATA.EF;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cStoreMVC.DOMAIN.Repositories
{
    public class GenericRepository<TEntity> :IGenericRepository<TEntity> where TEntity : class
    {
        //data context
        private cStoreEntities db = new cStoreEntities();

        //public virtual List<TEntity> Get()
        //{
        //    return db.Set<TEntity>().ToList();
        //}
        public virtual List<TEntity> Get(string includeProperties = "")
        {
            //This generic repository works fien for "simple" tables, but tables such as Titles (LINQ)
            //to include other properties....

            //1) Query base table
            IQueryable<TEntity> query = db.Set<TEntity>();
            //2) Loop through additional properties (passed as parameters)
            foreach (var prop in includeProperties.Split(new char[] { ',' }, 
                StringSplitOptions.RemoveEmptyEntries))
            {
                //3) include additional properties (tables) in the query
                query = query.Include(prop);
            }//foreach
            return query.ToList();
        }
        
        public TEntity Find(object id)
        {
            return db.Set<TEntity>().Find(id);
        }
        public void Add(TEntity entity)
        {
            db.Set<TEntity>().Add(entity);
            db.SaveChanges();
        }
        public void Update(TEntity entity)
        {
            db.Entry(entity).State = EntityState.Modified;
            db.SaveChanges();
        }

        public void Remove(TEntity entity)
        {
            db.Set<TEntity>().Remove(entity);
            db.SaveChanges();
        }
        public void Remove(object id)
        {
            var entity = db.Set<TEntity>().Find(id);
            Remove(entity);
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }//end disposing
            }//end !disposed
        }//end Dispose()
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public int countRecords()
        {
            return Get().Count();
        }
    }//end class
}//end namespace
