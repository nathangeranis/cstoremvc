﻿var books = [
    {
        id: 1,
        title: "The Eye of Minds",
        author: "James Dashner",
        price: 9.99
    },
    {
        id: 2,
        title: "Harold and the Purple Crayon",
        author: "Crockett Johnson",
        price: 6.95
    },
    {
        id: 3,
        title: "Essential C# 6.0",
        author: "Mark Michaelis & Eric Lippert",
        price: 46.99
    },
    {
        id: 4,
        title: "The Maze Runner",
        author: "James Dashner",
        price: 7.65
    },
    {
        id: 5,
        title: "Macbeth",
        author: "William Shakespeare",
        price: 6.5
    },
    {
        id: 6,
        title: "Triumph",
        author: "Jeremy Schaap",
        price: 11.45
    }];

//Create array to store user's cart info

var cart = [];

//add items to cart -- wired to <a> tage in HTML

function addToCart(id) {
    //IF they have not added any of the titles yet, set the qty to 1 and 
    //add the book to our array
    //otherwise add 1 to the qty
    var bookObj = books[id - 1];
    if (typeof bookObj.qty === 'undefined') {
        bookObj.qty = 1;
        cart.push(bookObj);
    } else {
        bookObj.qty = bookObj.qty + 1;
    }

    /*For testing purposes only*/
    //console.clear();
    var arrayLength = cart.length;
    //for (var i = 0; i < arrayLength; i++) {
    //    console.log(cart[i]);
    //}

    var cartQty = 0;
    for (var i = 0; i < arrayLength; i++) {
        cartQty += cart[i].qty;
    }

    document.getElementById('cart-notification').innerHTML = cartQty;
    document.getElementById('cart-contents').innerHTML = getCartContents();
}


function getCartContents() {
    var cartContent = "";
    var cartTotal = 0;
    //Display the title, autor, qty, price,  and total price for all cart contents

    for (var i = 0; i < cart.length; i++) {
        cartContent += cart[i].title + "<br>by " + cart[i].author + "<br>QTY " +
            cart[i].qty + " at " + cart[i].price + " ea.<br><br>";
        cartTotal += cart[i].qty * cart[i].price;
    }
    cartContent += "Cart Total: $" + cartTotal.toFixed(2);
    return cartContent;
}