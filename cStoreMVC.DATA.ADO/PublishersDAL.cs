﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient; //Added for SqlClient classes
// Provides access to all SqlClient classes needed for data retrieval/manupulation in a MSSQL dB
//(SqlConnection, SqlCommand, SqlDataReader, SqlParameter)
using cStoreMVC.DOMAIN.Models;
using System.Web.Configuration; //Added Physical reference to System.Web AND System.Configuration
//allowing us access to .Configuration for WebConfigurationManager
namespace cStoreMVC.DATA.ADO
{
    //DAL is Data Access Layer
    //Any items, structures, commands to access data should live here.
    public class PublishersDAL
    {
        //We want a field that represents the connection string
        //.WebConfigElement["Name of the Connection"].Property;
        string connString = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        #region Adding Data to our Website
        /*
         * ADO.NET - ActiveX Data Objects
         * -integration of data into ASP.NET websites (Active Server Pages)
         * -uses SQLClient classes (.NET Framework, C#)
         * 
         * 1. Where is the Data?
         * 2. Send a request for the data/What do we want? (SELECT)
         * 
         */
        #endregion
        public string GetPublisherNames()
        {
            string publisherNames = "";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = @"Data Source=.\sqlexpress;Initial Catalog=cStore;Integrated Security = true;";

            conn.Open();

            SqlCommand cmdGetPublishers = new SqlCommand("SELECT * FROM Publishers", conn);


            SqlDataReader rdrPublishers = cmdGetPublishers.ExecuteReader();

            while (rdrPublishers.Read())
            {
                //while there is Data to read
                //Add current value to firstNames variable
                publisherNames += (string)rdrPublishers["PublisherName"] + ", ";
            }

            //6. Close the data Reader and db connection
            rdrPublishers.Close();//Close DRO
            conn.Close();//Close Connection

            //7. Return the results


            return publisherNames;
        }//GetFirstNames()

        public List<PublisherModel> GetPublishers()
        {
            List<PublisherModel> Publishers = new List<PublisherModel>();

            using (SqlConnection conn = new SqlConnection())
            {
                //Do NOT want open dB connections "hanging around".....
                //Wrapping the dB connection in a using block allows for automatically closing the 
                //connection once it's no longer being used

                //Using () will automatically close the connection object, however we still 
                //need to manually open the connection
                //conn.ConnectionString = @"Data Source=.\sqlexpress;Initial Catalog=cStore;Integrated Security=true;";
                //Now using updated ConnectionString object.
                conn.ConnectionString = connString;
                conn.Open();
                SqlCommand cmdGetAllPublishers = new SqlCommand("SELECT * FROM Publishers", conn);

                //Execute the command and store the results

                SqlDataReader rdrPublishers = cmdGetAllPublishers.ExecuteReader();
                while (rdrPublishers.Read())
                {
                    //create an PublisherModel object that can hold all of the info for an Publisher
                    PublisherModel Publisher = new PublisherModel()
                    {
                        //Assign values to PublisherModel properties
                        PublisherID = (int)rdrPublishers["PublisherID"],
                        PublisherName = (string)rdrPublishers["PublisherName"],
                        City = (rdrPublishers["City"] is DBNull) ? "N/A" : (string)rdrPublishers["City"],
                        State = (rdrPublishers["State"] is DBNull) ? "N/A" : (string)rdrPublishers["State"]
                    };//end Publisher
                    Publishers.Add(Publisher);
                }//end while
                rdrPublishers.Close();
            }//using





            return Publishers;
        }//GetPublishers()
        public void CreatePublisher(PublisherModel publisher)
        {
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                SqlCommand cmdInsertPublisher = new SqlCommand(
                    "INSERT INTO Publishers(PublisherName, City, State)" +
                    "VALUES(@PublisherName, @City, @State)", conn);
                //Creating a SqlParameter object to pass into ADD()
                //SqlParameter FirstName = new SqlParameter("@FirstName", Publisher.FirstName);
                //cmdInsertPublisher.Parameters.Add(FirstName);

                //SqlParameter LastName = new SqlParameter("@LastName", Publisher.LastName);
                //cmdInsertPublisher.Parameters.Add(LastName);

                //SqlParameter City = new SqlParameter("@City", Publisher.City);
                //cmdInsertPublisher.Parameters.Add(City);

                //SqlParameter State = new SqlParameter("@State", Publisher.State);
                //cmdInsertPublisher.Parameters.Add(State);

                //SqlParameter ZipCode = new SqlParameter("@ZipCode", Publisher.ZipCode);
                //cmdInsertPublisher.Parameters.Add(ZipCode);

                //Add variables + values into the SqlCommand object
                //Seen frequently in production level code

                cmdInsertPublisher.Parameters.AddWithValue("@PublisherName", publisher.PublisherName);
                if (publisher.City != null)
                {
                    cmdInsertPublisher.Parameters.AddWithValue("@City", publisher.City);
                }
                else
                {
                    cmdInsertPublisher.Parameters.AddWithValue("@City", DBNull.Value);
                }
                if (publisher.State != null)
                {

                }
                cmdInsertPublisher.Parameters.AddWithValue("@State", publisher.State);
                cmdInsertPublisher.ExecuteNonQuery();
            }//using
        }//CreatePublisher()

        public PublisherModel FindPublisher(int ID)
        {
            //Accept an ID as a parameter to know which publisher to find (or to update)
            PublisherModel publisher = null;
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                SqlCommand cmdFindPublisher = new SqlCommand(
                    "SELECT * FROM Publishers WHERE PublisherID = @PublisherID", conn);
                //Add SqlParameter for @PublisherID
                cmdFindPublisher.Parameters.AddWithValue("@PublisherID", ID);
                //Execute the command
                SqlDataReader rdr = cmdFindPublisher.ExecuteReader();
                if (rdr.Read())
                {
                    //An publisher was returned and we should build our PublisherModel object
                    publisher = new PublisherModel()
                    {
                        PublisherID = (int)rdr["PublisherID"],
                        PublisherName = (string)rdr["PublisherName"],
                        City = (rdr["City"] is DBNull) ? "N/A" : (string)rdr["City"],
                        State = (rdr["State"] is DBNull) ? "N/A" : (string)rdr["State"]

                    };
                }
                rdr.Close();
            }
            return publisher;
        }//end FindPublisher()
        public void UpdatePublisher(PublisherModel publisher)
        {
            using (SqlConnection conn = new SqlConnection(connString))
            {
                SqlCommand cmdUpdatePublisher = new SqlCommand(
                    @"UPDATE Publishers SET
                        PublisherName = @PublisherName, 
                        City = @City,
                        State = @State
                        WHERE PublisherID = @PublisherID", conn);
                cmdUpdatePublisher.Parameters.AddWithValue("@PublisherID", publisher.PublisherID);
                cmdUpdatePublisher.Parameters.AddWithValue("@PublisherName", publisher.PublisherName);

                //Nullable Fields
                if (publisher.City != null)
                {
                    cmdUpdatePublisher.Parameters.AddWithValue("@City", publisher.City);
                }
                else
                {
                    cmdUpdatePublisher.Parameters.AddWithValue("@City", DBNull.Value);
                }
                if (publisher.State != null)
                {
                    cmdUpdatePublisher.Parameters.AddWithValue("@State", publisher.State);
                }
                else
                {
                    cmdUpdatePublisher.Parameters.AddWithValue("@State", DBNull.Value);
                }
                conn.Open();
                cmdUpdatePublisher.ExecuteNonQuery();
            }//using
        }//UpdatePublisher
        public void DeletePublisher(int id)
        {
            /*
             * HARD DELETE: 
             * DeletePublisher() is a HARD Delete - Meaning, this will remove the item from the dB as long as
             * it is NOT bound by a primary/foreign key relationship.
             * 
             *SOFT DELETE:
             *REALLY just an update statement that changes the status field in a table. The field could be a bit value 
             * in the table (ex. discontinued-Northwind) or it could be a foreign key to a lookup table ex. AcctStatusCode
             * -F2071 Bank) *****In the case of foreign key, it takes a little more work in the update (more logic)
             * 
             */

            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();

                SqlCommand cmdDeletePublisher = new SqlCommand("DELETE FROM Publishers WHERE PublisherID = @PublisherID", conn);
                /*******************DO NOT FORGET YOUR WHERE CLAUSE***********************/
                cmdDeletePublisher.Parameters.AddWithValue("@PublisherID", id);
                cmdDeletePublisher.ExecuteNonQuery();
            }//using
        }

    }//end class
}//end namespace
