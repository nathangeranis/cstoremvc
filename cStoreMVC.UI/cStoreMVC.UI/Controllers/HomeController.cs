﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using cStoreMVC.UI.Models;
using System.Net.Mail;//Required for MailMessage object and SMTPClient
using System.Net;
using System.Diagnostics;//Diagnostic Methods
/*
 * Debugging:
 * 
 * To launch in debug mode, execute the application with F5.
 * Set Breakpoints wherever you want the application to pause
 * Use Step Into (F11) to go through the code line by line
 * -This will also enter any custom methods being called
 * 
 * Step Out(Shift + F11) will finish processing a method that has been stepped into.
 * 
 * Step Over(F10) will execute the code in a method being called without entering it to step through line by line.
 * 
 * 
 */



namespace cStoreMVC.UI.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult FAQ()
        {

            return View();
        }
        //This is the GET
        public ActionResult Contact()
        {

            return View();
        }

        //POST
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Contact(ContactViewModel contact)
        {
            //Create the body for the E-Mail
            string body = string.Format($"Name: {contact.Name}<br>" + $"Email: {contact.Email}<br>Subject: {contact.Subject} " +
                $"Message:<br>{contact.Message}");
            Debug.WriteLine(body);
            //Create and configure the Mail Message Object
            MailMessage msg = new MailMessage("no-reply@thecodegym.com", //From address on hosting account, 
                "nathangeranis@outlook.com", //To Address where teh email will be delivered,
                contact.Subject + "-" + DateTime.Now, body);
            //Extra Configuration
            msg.Priority = MailPriority.High;
            msg.IsBodyHtml = true;
            msg.CC.Add("nathangeranis@outlook.com");

            //Create and configure the SMTP Client
            SmtpClient client = new SmtpClient("mail.thecodegym.com");
            client.Credentials = new NetworkCredential("no-reply@thecodegym.com", "P@ssw0rd!23");
            //Attempt to send the email
            using (client)
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        client.Send(msg);
                    }
                    else
                    {
                        return View();
                    }
                }
                catch 
                {
                    ViewBag.ErrorMessage = "There was a problem. Please try again.";
                    return View();
                }
            }

            return View("ContactConfirmation", contact);
        }
    }
}