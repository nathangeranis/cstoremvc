﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace cStoreMVC.DATA.EF.METADATA
{
    public class MagazinesMetadata
    {

        public string MagazineTitle { get; set; }


        public int IssuesPerYear { get; set; }


        public decimal PricePerYear { get; set; }


        public string Category { get; set; }


        public Nullable<int> Circulation { get; set; }


        public string PublishRate { get; set; }
    }//end class
    [MetadataType(typeof(MagazinesMetadata))]
    public partial class Magazine { }
}//end namespace
